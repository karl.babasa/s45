import React, {useState, useEffect, useContext} from 'react';
import {Form, Button} from	'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom'

export default function	Register() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	//state hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1 , setPassword1] = useState('');
	const [password2 , setPassword2] = useState('');
	const [firstName , setFirstName] = useState('');
	const [lastname , setLastName] = useState('');
	const [gender, setGender] = useState('');
	const [mobileNumber , setMobileNumber] = useState('');

	//State to determine whether submit is enable or not for conditional rendering
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		//Validation to enable submit button when all field are papulated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== "" && lastname !== "" && gender !== "" && mobileNumber !== "") && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, setPassword2, setFirstName, setLastName, setGender, setMobileNumber])

	function registerUser(e) {
		e.preventDefault()

		fetch('https://b165-shared-api.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password1,
				firstName: firstName,
				lastName: lastname,
				gender: gender,
				mobileNo: mobileNumber
 
			})

		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			Swal.fire({
			title: "Yay!",
			icon: 'success',
			text: `Successfully Registered an Account`
			})
			
			navigate('/login')

			setEmail('');
			setPassword1('');
			setPassword2('');
			setFirstName('');
			setLastName('');
			setGender('');
			setMobileNumber('');
		})
	}

	return(

		//Conditional Rendering
		(user.accessToken !== null) ?

		<Navigate to = "/"/>

		:

		<Form onSubmit={(e) => registerUser(e)}>
		<h1>Register</h1>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
				type="email"
				placeholder="Enter Email Address"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
				type="password"
				placeholder="Enter Password"
				required
				value={password1}
				onChange={e => setPassword1(e.target.value)}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
				type="password"
				placeholder="Verify Password"
				required
				value={password2}
				onChange={e => setPassword2(e.target.value)}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control 
				type="text"
				placeholder="First Name"
				required
				value={firstName}
				onChange={e => setFirstName(e.target.value)}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
				type="text"
				placeholder="Last Name"
				required
				value={lastname}
				onChange={e => setLastName(e.target.value)}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Gender</Form.Label>
				<Form.Control 
				type="text"
				placeholder="Male/Female"
				required
				value={gender}
				onChange={e => setGender(e.target.value)}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
				type="text"
				placeholder="Mobile Number"
				required
				value={mobileNumber}
				onChange={e => setMobileNumber(e.target.value)}/>
			</Form.Group>

			{isActive ?
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>

				:

				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
			}
			
		</Form>

		)
}