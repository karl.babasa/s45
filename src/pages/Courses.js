import React, {useContext, useState, useEffect} from 'react';
//import coursesData from '../mockData/coursesData';
//import CourseCard from '../components/CourseCard';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';

import UserContext from '../UserContext'

export default function Courses() {
	//for us to e able to display all the courses from the data file, we are going to use map()
	//The "map" method loops through all the indevidual course object in our array and return a component for each course.
	//Multiple components created through the map method mush have aUNIQUE KEY that will help React JS identify which components/elements have been changed, added or removed.
/*	const courses = coursesData.map(course => {
		return(
			<CourseCard key={course.id}courseProp={course}/>
			)
	})
	*/

	const {user} = useContext(UserContext);

	const[allCourses, setAllCourses] = useState([])

	const fetchData = () => {
		fetch('https://b165-shared-api.herokuapp.com/courses/')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllCourses(data)
		})
	}

	useEffect(() => {
		fetchData()
	}, [])

	return(
		<>
			{(user.isAdmin === true) ?

				<AdminView coursesData = {allCourses} fetchData={fetchData}/>

				:

				<UserView coursesData = {allCourses}/>

			}
		</>
		)
}