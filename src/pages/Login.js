/*import React, {useState, useEffect} from 'react';
import	{Form, Button} from	'react-bootstrap';
import Swal from 'sweetalert2';

export default function	Login() {

	const [email, setEmail] = useState('');
	const [password , setPassword] = useState('');
	
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		//Validation to enable submit button when all field are papulated and both passwords match
		if((email !== '' && password !== '')){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	function loginUser(e) {
		//Prevents page redirection via form submission
		e.preventDefault();

		//clear input
		setEmail('')
		setPassword('')

		Swal.fire({
			title: "Login",
			icon: "success",
			text: "Login: True"
		})
	}

	return(
		<Form onSubmit={(e) => loginUser(e)}>
			<h1>Login</h1>

			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
				type="email"
				placeholder="Enter Email Address"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
				type="password"
				placeholder="Enter Password"
				required
				value={password}
				onChange={e => setPassword(e.target.value)}/>
			</Form.Group>

			{isActive ?
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>

				:

				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
			}
		</Form>
		)

}*/
import React, {useState, useEffect, useContext} from 'react';
//useContext is used to unpack or deconstruct the value of the UserContext
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom'

export default function Login() {

	const navigate = useNavigate();


	//Allows us to consume the User context object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	const [isActive, setIsActive] = useState(true)

	useEffect(() => {
		if((email !== '' && password !== '')){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])


	/*Notes:
		fetch() is a method in JS, which allows to send arequest to an API and process its response.

		SYNTAX: 

		fetch('url',{options}).then(responce => response.json()).then(data => {console.log(data)})

		-'url' = the url coming from the API/server
		-{optional object} = it contains additional information about our requests such as method, body and headers (Content-Type: application/json) or any other info.
		-then(response => response.json()) = parse the response as JSON
		-then(data => {console.log(data)}) = process the actual data

		Note:
		no option means 'GET'

		*/

	function authenticate(e) {
		e.preventDefault();

		fetch('https://b165-shared-api.herokuapp.com/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if (data.accessToken !== undefined) {
				localStorage.setItem('accessToken', data.accessToken);
				setUser({accessToken: data.accessToken})

				Swal.fire({
				title: "Yay!",
				icon: 'success',
				text: `${email} Successfully logged in`
				})
				//Getting the user's credentials
				fetch('https://b165-shared-api.herokuapp.com/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result)
					if(result.isAdmin === true) {
						localStorage.setItem('email', result.email)
						localStorage.setItem('isAdmin', result.isAdmin)
						setUser({
							email: result.email,
							isAdmin: result.isAdmin
						})

						navigate('/courses')
					} else {
						navigate('/')
					}
				})
			} else {

				Swal.fire({
				title: "Ooops!",
				icon: 'error',
				text: `Something went wrong, Check your Credentials.`
				})
			}

			setPassword('');

		})
	}

		/*//Set the email of the authenticated user in the localStorage
		//localStorage.setItem('propertyName', value)
		localStorage.setItem('email', email);

		//Set the global user state to have properties obtained from localStorage
		setUser({
			email: localStorage.getItem('email')
		})

		Swal.fire({
			title: "Yay!",
			icon: 'success',
			text: `${email} Successfully logged in`
		})

		setEmail('');
		setPassword('');*/
	
	return(
		//create the contditional rendering statement that will redirect the user to the course page when a user is logged in.

		(user.accessToken !== null) ?
		<Navigate to = "/"/>

		:

		<Form onSubmit={(e) => authenticate(e)}>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
				type="email"
				placeholder="Enter Email Address"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
				type="password"
				placeholder="Enter Password"
				required
				value={password}
				onChange={e => setPassword(e.target.value)}/>
			</Form.Group>

			{isActive ?
				<Button variant="primary" type="submit" className="my-3">Login</Button>

				:

				<Button variant="danger" type="submit" className="my-3" disabled>Login</Button>
			}
 		</Form>
		)
}
