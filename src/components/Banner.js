import React from 'react';
import {Row, Col, Button} from 'react-bootstrap';

export default function Banner() {


	return(

	<Row>
		<Col className="p-5">
			<h1 className="mb-3">Karl Babasa Course Booking</h1>
			<p className="my-3">Oppotunities for everyone, everywhere</p>
			<Button variant="primary">Enroll now!</Button>
		</Col>
	</Row>

		)
}