import React, {useState} from 'react';
import './App.css';
import AppNavBar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Login from './pages/Login';
import Logout from './pages/Logout';
import SpecificCourse from './pages/SpecificCourse';
import Register from './pages/Register';
import Error from './pages/Error';
import {Container} from 'react-bootstrap';
import {UserProvider} from './UserContext'

//For routes

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

//The Router(BrowserRouter) conponent will enable us to simulate page navigation by syncronizaing he shown content and the shwon URL in the web Browser.

//The Routes(before it is calls Switch)

function App() {

  //const UserContext = React.createContext()

    //React context os nothing but a global state to the app. It is a way to make a partcular data available to all the components no matter how the are nested.
    //Context helps you broadcast data and changes happening to that data/state to all components

    const [user, setUser] = useState({
      accessToken: localStorage.getItem('accessToken'),
      email: localStorage.getItem('email'),
      isAdmin: localStorage.getItem('isAdmin') === 'true'
    })

    //Function for clearing localStorage on logout
    const unsetUser = () => {
      localStorage.clear()
    }

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/courses" element={<Courses/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/courses/:courseId" element={<SpecificCourse/>} />
            <Route path="*" element={<Error/>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}   

export default App;

//fo adding Javascript(Babel) Linting in sublime text
// ctrl shift P > tpe install > click the Package Control Install > Babel


